﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="EasyRestaurantWeb.Vistas.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- CSS -->
    <link href="../resources/css/estilos.css" rel="stylesheet" />
    <link href="../Content/site.css" rel="stylesheet" />
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>    

</head>
<body>
    <div class="container-fluid">
  <div class="row no-gutter">
    <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>
    <div class="col-md-8 col-lg-6">
      <div class="login d-flex align-items-center py-5">
        <div class="container">
          <div class="row">
            <div class="col-md-9 col-lg-8 mx-auto">
              <h3 class="login-heading mb-4">Easy Restaurant</h3>
              <form runat="server">
                <div class="form-label-group">
                    <!-- Ingreso de RUT -->
                  <asp:TextBox ID="txtRut" runat="server" class="form-control"></asp:TextBox>
                  <label for="inputEmail">Rut</label>
                </div>

                <div class="form-label-group">
                    <!-- Ingreso de Pass -->
                  <asp:TextBox ID="txtPass" runat="server" class="form-control"></asp:TextBox>
                  <label for="inputPassword">Clave</label>
                </div>
                <div class="custom-control custom-checkbox mb-3"> 
                </div>
                  <!-- Boton Ingreso -->
                 <asp:Button ID="btnInicioSesion" runat="server" Text="Iniciar Sesión" class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" OnClick="btnInicioSesion_Click" />
                <div class="text-center"> </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
</div>
</body>
</html>
