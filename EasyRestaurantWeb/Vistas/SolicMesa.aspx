﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SolicMesa.aspx.cs" Inherits="EasyRestaurantWeb.Vistas.SolicMesa" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- CSS -->
    <link href="../resources/css/estilos.css" rel="stylesheet" />
    <link href="../Content/site.css" rel="stylesheet" />
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>
<body>
    <!-- NavBar -->
    <header>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample08">
                <ul class="navbar-nav">
                    <li class="nav-item active nadbar">
                        <h1>
                            <a class="nav-link">Solicitar Mesa</a>
                        </h1>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <form id="form1" runat="server">
            <!-- Drop Cantidad de Personas -->
            <div class="text-center">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    Cantidad Personas
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">Mesa 2 Personas</a>
                    <a class="dropdown-item" href="#">Mesa 4 Personas</a>
                    <a class="dropdown-item" href="#">Mesa 6 Personas</a>
                    <a class="dropdown-item" href="#">Mesa 8 Personas</a>
                    <a class="dropdown-item" href="#">Mesa 10 Personas</a>
                </div>
            </div>
        <div class="container">
            <h2></h2>
            <p></p>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>N° Mesa</th>
                        <th>Estado</th>
                        <th>Ubicación</th>
                        <th>Pedir</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Mesa 1</td>
                        <td>Disponible</td>
                        <td>Comedor</td>
                        <td>
                            <asp:Button ID="btnSolicitar1" runat="server" Text="Solicitar" />
                        </td>
                    </tr>
                    <tr>
                        <td>Mesa 2</td>
                        <td>Ocupada</td>
                        <td>Terraza</td><td>
                            <asp:Button ID="btnSolicitar2" runat="server" Text="Solicitar" />
                        </td>
                    </tr>
                    <tr>
                        <td>Mesa 3</td>
                        <td>Disponible</td>
                        <td>Comedor</td>
                         <td>
                            <asp:Button ID="btnSolicitar3" runat="server" Text="Solicitar" />
                        </td>
                    </tr>
                    <tr>
                        <td>Mesa 4</td>
                        <td>Disponible</td>
                        <td>Comedor</td>
                        <td>
                            <asp:Button ID="btnSolicitar4" runat="server" Text="Solicitar" />
                        </td>
                    </tr>
                     <tr>
                        <td>Mesa 5</td>
                        <td>Disponible</td>
                        <td>Terraza</td>
                        <td>
                            <asp:Button ID="btnSolicitar5" runat="server" Text="Solicitar" />
                        </td>
                    </tr>
                    <tr>
                        <td>Mesa 6</td>
                        <td>Ocupada</td>
                        <td>Comedor</td>
                        <td>
                            <asp:Button ID="btnSolicitar6" runat="server" Text="Solicitar" />
                        </td>
                    </tr>
                     <tr>
                        <td>Mesa 7</td>
                        <td>Disponible</td>
                        <td>Terraza</td>
                        <td>
                            <asp:Button ID="btnSolicitar7" runat="server" Text="Solicitar" />
                        </td>
                    </tr>
                    <tr>
                        <td>Mesa 8</td>
                        <td>Disponible</td>
                        <td>Terraza</td>
                        <td>
                            <asp:Button ID="btnSolicitar8" runat="server" Text="Solicitar" />
                        </td>
                    </tr>
                    <tr>
                        <td>Mesa 9</td>
                        <td>Disponible</td>
                        <td>Terraza</td>
                        <td>
                            <asp:Button ID="btnSolicitar9" runat="server" Text="Solicitar" />
                        </td>
                    </tr>
                     <tr>
                        <td>Mesa 10</td>
                        <td>Ocupada</td>
                        <td>Terraza</td>
                        <td>
                            <asp:Button ID="btnSolicitar10" runat="server" Text="Solicitar" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
